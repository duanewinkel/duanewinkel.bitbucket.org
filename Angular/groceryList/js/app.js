/**
 * Created by DW on 5/6/15.
 */
var myApp = angular.module('myApp', []);
myApp.controller('GroceryController', function($scope){
    $scope.itemName;

    //array of items
    $scope.itemArray = ['Beer', 'Wine', 'Bourbon', "Wings"];

    //add items to list
    $scope.addItem = function(){
        $scope.itemArray.push($scope.itemName);

        $scope.itemName = '';
    };

    //delete list items

    $scope.deleteItem = function(deletedItem){
        var idx = $scope.itemArray.indexOf(deletedItem);
        $scope.itemArray.splice(idx, 1);
    }

});


//todo: form validation needs to be added can't add the same element twice, can't add empty items

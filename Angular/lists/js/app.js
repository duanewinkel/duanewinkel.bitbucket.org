/**
 * Created by DW on 5/19/15.
 */

var apples = angular.module('YourApp',['ngRoute'])

        //Route section
    .config(function($routeProvider){
        $routeProvider.when('/view1',{
            templateUrl : "view1.html",
            controller : 'BookController'
        }).when('/view2',{
            templateUrl : 'view2.html',
            controller : 'MovieController'
        }).when("/view3",{
            templateUrl : 'view3.html',
            controller : 'ConcertController'
        }).otherwise({
            redirectTo : '/view1'
        });
})

//controllers
//    BOOK CONTROLLER
    .controller("BookController", function($scope,DataService){
        console.log("test view2");

        $scope.books = DataService.getBook();
        $scope.newBook ={};


        $scope.addBook = function(){
            DataService.saveBook($scope.newBook);

            $scope.newBook = '';
            console.log('add new book here');
        };

        $scope.eraseBook =function(eraseBook){
            DataService.removeBook(eraseBook)

        };
    })
//PAGE 2 CONTROLLER
    .controller("MovieController", function($scope,DataService){
        $scope.movies = DataService.getMovie();
        $scope.newMovie ={};


        $scope.addMovie = function(){
            DataService.saveMovie($scope.newMovie);

            $scope.newMovie = '';
            console.log('inside addmovie!');
        };

        $scope.eraseMovie =function(eraseMovie){
            DataService.removeMovie(eraseMovie)

        };
    })

//Page 3
    .controller("ConcertController", function($scope,DataService){
        $scope.concerts = DataService.getConcert();
        $scope.newConcert ={};


        $scope.addConcert = function(){
            DataService.saveConcert($scope.newConcert);

            $scope.newConcert = '';
            console.log('Inside addConcert!');
        };

        $scope.eraseConcert =function(eraseConcert){
            DataService.removeConcert(eraseConcert)

        };

    });


//main controller
    apples.controller('MyAppController', function($scope){
       $scope.test= "Hello World";

    });






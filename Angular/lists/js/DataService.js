/**
 * Created by DW on 5/13/15.
 */


var apples = angular.module('YourApp');
    apples.service("DataService",function(){

   var bookArray =[
       {
           "author": "J. R. R. Tolken",
           "titleOfBook": 'There and Back Again',
           "date": "1996"

       },
       {
           "author": "J. R. R. Tolken",
           "titleOfBook": "Fellowship of the Ring",
           "date": "1995"
       },
       {
           "author": "J. R. R. Tolken",
           "titleOfBook": 'Return of the King',
           "date": "1997"
       }
   ];


    this.getBook = function(){
        var str = localStorage.getItem('book');
        bookArray = JSON.parse(str) || bookArray;
        console.log('#2!');

        return bookArray;
    };

    this.saveBook = function(bIndex){
        bookArray.push(bIndex);
        var str = JSON.stringify(bookArray);
        localStorage.setItem("book", str);
    };

    this.removeBook =function(bIndex){
        bookArray.splice(bookArray.indexOf(bIndex),1);
        var str = JSON.stringify(bookArray);
        localStorage.setItem("book", str);
    };


        var movieArray =[
            {
                "movieTitle": "Return of the Jedi",
                "actor": 'Mark Hamill',
                "dateWatched": "1983"
            },
            {
                "movieTitle": "Empire Strikes Back",
                "actor": 'Carrie Fisher',
                "dateWatched": "1980"
            },
            {
                "movieTitle": "A New Hope",
                "actor": 'Harrison Ford',
                "dateWatched": "1977"
            }
        ];


        this.getMovie = function(){
            var str = localStorage.getItem('movie');
            movieArray = JSON.parse(str) || movieArray;
            console.log('##3');

            return movieArray;
        };

        this.saveMovie = function(mIndex){
            movieArray.push(mIndex);
            var str = JSON.stringify(movieArray);
            localStorage.setItem("movie", str);
        };

        this.removeMovie =function(mIndex){
            movieArray.splice(movieArray.indexOf(mIndex),1);
            var str = JSON.stringify(movieArray);
            localStorage.setItem("movie", str);
        };


        var concertArray =[
            {
                "headliner": "POD",
                "openingAct": 'Superchick',
                "dateAttended": "2008"
            },
            {
                "headliner": "Carrie Underwood",
                "openingAct": 'Hunter Hayes',
                "dateAttended": "2010"
            },
            {
                "headliner": "Distrubed",
                "openingAct": 'Breaking Benjamin',
                "dateAttended": "2007"
            }
        ];


        this.getConcert = function(){
            var str = localStorage.getItem('concert');
            concertArray = JSON.parse(str) || concertArray;
            console.log('#4?');

            return concertArray;
        };

        this.saveConcert = function(cIndex){
            concertArray.push(cIndex);
            var str = JSON.stringify(concertArray);
            localStorage.setItem("concert", str);
        };

        this.removeConcert =function(cIndex){
            concertArray.splice(concertArray.indexOf(cIndex),1);
            var str = JSON.stringify(concertArray);
            localStorage.setItem("concert", str);
        };


});